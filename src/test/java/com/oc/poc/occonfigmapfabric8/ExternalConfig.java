package com.oc.poc.occonfigmapfabric8;

import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;

/**
 * Created by pchaivong on 11/23/2017 AD.
 */

@Configuration
@ActiveProfiles("TEST")
public class ExternalConfig {
    private long balance = 0;
}
