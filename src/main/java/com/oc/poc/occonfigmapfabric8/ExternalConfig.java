package com.oc.poc.occonfigmapfabric8;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by pchaivong on 11/23/2017 AD.
 */

@Getter
@Setter
@NoArgsConstructor
@Configuration
@Profile("PROD")
@PropertySource("/conf/application.properties")
@ConfigurationProperties(prefix = "external")
public class ExternalConfig {

    private long balance;
}
