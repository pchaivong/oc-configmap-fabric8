package com.oc.poc.occonfigmapfabric8;

import lombok.*;

/**
 * Created by pchaivong on 11/8/2017 AD.
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class MessageDTO {

    private String message;
}
