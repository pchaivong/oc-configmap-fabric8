package com.oc.poc.occonfigmapfabric8;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by pchaivong on 11/23/2017 AD.
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BalanceDTO {
    private long balance;
}
