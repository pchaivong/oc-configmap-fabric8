package com.oc.poc.occonfigmapfabric8;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by pchaivong on 11/8/2017 AD.
 */

@Configuration
@ConfigurationProperties(prefix = "fabric8")
@NoArgsConstructor
@Getter
@Setter
public class MessageConfig {

    private String message;
}
