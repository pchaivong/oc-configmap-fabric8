package com.oc.poc.occonfigmapfabric8;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class OcConfigmapFabric8Application {

	public static void main(String[] args) {
		SpringApplication.run(OcConfigmapFabric8Application.class, args);
	}
}
