package com.oc.poc.occonfigmapfabric8;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by pchaivong on 11/8/2017 AD.
 */


@RestController
public class MessageController {

    @Autowired
    private MessageConfig messageConfig;

    @GetMapping("/message")
    public MessageDTO getMessage(){
        return new MessageDTO(messageConfig.getMessage());
    }

    /*
    @GetMapping("/balance")
    public BalanceDTO getBalance(){
        return new BalanceDTO(externalConfig.getBalance());
    }
    */
}
